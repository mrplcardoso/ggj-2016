﻿using UnityEngine;
using System.Collections;

public class MovePeixe : MonoBehaviour
{
  float i = 10, x = 30;
  float j = 0;
  public Transform center, tp1, tp2;

  void Start()
  {
    transform.position = center.position;
  }

  // Update is called once per frame
  void Update()
  {
    StartCoroutine("Move");
  }

  IEnumerator Move()
  {
    transform.Rotate(new Vector3(0, 0, 1), i * Time.deltaTime);
    transform.Rotate(new Vector3(0, 1, 0), x * Time.deltaTime);

    GetComponentInChildren<Transform>().gameObject.transform.position = 
      CubicBezier(j, tp1.position, tp2.position, tp2.position, tp1.position);
    j += Time.deltaTime / 2;
    if(j > 1)
      j = 0;
    yield return null;
  }

  public Vector3 CubicBezier(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
  {
    return (Mathf.Pow(1 - t, 3) * p0
      + 3 * Mathf.Pow(1 - t, 2) * t * p1
      + 3 * (1 - t) * t * t * p2
      + t * t * t * p3);
  }
}
