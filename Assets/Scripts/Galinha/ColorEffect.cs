﻿using UnityEngine;
using System.Collections;

public class ColorEffect : MonoBehaviour
{
  //controla quando aumenta ou abaixa o alfa
  [HideInInspector]
  public bool alpha = false;

  [HideInInspector]
  public MeshRenderer floor;
  Color c;

  void Start()
  {
    floor = GetComponent<MeshRenderer>();
  }

  void Update()
  {
    if (!alpha)
      StartCoroutine("FadeIn");
    if (alpha)
      StartCoroutine("FadeOut");
  }

  IEnumerator FadeOut()
  {
    c = floor.material.color;
    c.a -= 0.01f;
    floor.material.color = c;

    if (c.a < 0f)
      alpha = false;
    yield return new WaitForSeconds(5);
  }

  IEnumerator FadeIn()
  {
    c = floor.material.color;
    c.a += 0.01f;
    floor.material.color = c;

    if (c.a > 1f)
      alpha = true;
    yield return new WaitForSeconds(5);
  }
}
