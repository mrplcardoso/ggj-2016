﻿using UnityEngine;
using System.Collections;

public class Run : MonoBehaviour
{
  public GameObject player;

  //Vetor de fuga
  [HideInInspector]
  public Vector3 runDir;

  [HideInInspector]
  public float mVel = 5;

  private Rigidbody body;

  void Awake()
  {
    body = GetComponent<Rigidbody>();
  }
  void Update()
  {
    StartCoroutine("RunMoviment");
  }

  IEnumerator RunMoviment()
  {
    if(Vector3.Distance(
      player.transform.position,
      transform.position) < 3f)
    {
      Debug.Log(runDir = (transform.position - player.transform.position).normalized);
      Debug.Log(body.position += runDir * Time.deltaTime * mVel);
      Debug.Log(transform.localEulerAngles = new Vector3(0, 1 * runDir.y, 0));
    }
    yield return null;
  }

  void OnTriggerEnter(Collider col)
  {
    if (col.gameObject.CompareTag("floor"))
      mVel = 2;
    else
      mVel = 5;
  }
}
