﻿using UnityEngine;
using System.Collections;

public class StartPlayer : MonoBehaviour {

    public PlayerController player;
    
	public void StartingPlayer () {
        player.enabled = true;
	}

    public void HideInfo(GameObject info)
    {
        info.SetActive(false);
    }
}
