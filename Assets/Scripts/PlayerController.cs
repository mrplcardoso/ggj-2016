﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    private Animator animator;

    public float speed = 10.0F;
    public float rotationSpeed = 100.0F;
    public bool fire, bowl, daime, chicken;
    public Run runChicken;
    public GameObject info, Fogueira, finalFogueira, argila, cumbuca, chickenFly;
    public Transform posicaoFogueira, finalPositionFogueira, posicaoargila;
    public int itensFire;
    public AudioClip acenderFogo, pegaGalinha, Madeira, tenebroso;

    public PlaySounds play;

    void Start ()
    {
        animator = GetComponent<Animator>();
    }
    
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        float translation = z * speed;
        float rotation = x * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);

        animator.SetFloat("X", x);
        animator.SetFloat("Z", z);

        if (itensFire >= 5)
        {
            StartFogueira();
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Fire"))//fase do fogo
        {
            Info("Find straw and wood in the forest to do the Ritual!");
            other.GetComponent<CreateFire>().cria();
        }
        else if (other.CompareTag("Bowl") && fire)//fase da tigela
        {
            Info("Get clay on the river and use it with fire to forge a bowl");
            Instantiate(argila, posicaoargila.position, posicaoargila.rotation);
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Daime") && fire && bowl)//fase do cha
        {
            Info("Cook the holy tea! - Grab some water, the bowl and liana");
        }
        else if (other.CompareTag("Chicken") && fire && bowl && daime)
        {
            Info("Catch the black chicken!");
            //runChicken.enabled = true;
        }
        else if (other.CompareTag("Bowl") || other.CompareTag("Daime") || other.CompareTag("Chicken"))
        {
            Info("You still don't have enough experience to practice the ritual! First, find the others rituals");
        }

        if (other.CompareTag("fireItens"))
        {
            itensFire++;
            Destroy(other.gameObject);
            play.Play(Madeira);
        }

        if (other.CompareTag("fogueira"))
        {
            fire = true;
            
            if (bowl)
            {
                Instantiate(cumbuca, posicaoFogueira.position, posicaoFogueira.rotation);
                Info("Congratlations! You have just completed the Erth Ritual! Tip: there is another ritual is about chicken...");
                chicken = true;
            }
            else if (chicken)
            {
                play.Play(tenebroso);
                Info("Congratlations! You have just completed the Chicken Black Ritual!");
                chickenFly.SetActive(true);
                gameObject.SetActive(false);
            }
            else
            {
                play.Play(acenderFogo);
                Instantiate(Fogueira, posicaoFogueira.position, posicaoFogueira.rotation);
                Info("Congratlations! You have just completed the Fire Ritual! Tip: there is another ritual next to water ...");
            }
                
            Destroy(other.gameObject);
        }

        if (other.CompareTag("Argila"))
        {
            bowl = true;
            StartFogueira();
            Destroy(other.gameObject);
        }

        if (other.CompareTag("galinha") && chicken)
        {

            play.Play(pegaGalinha);
            daime = true;
            
            Destroy(other.gameObject);
            chickenFly.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    void Info(string text)
    {
        info.transform.parent.gameObject.SetActive(true);
        info.GetComponent<Text>().text = text;
    }

    void StartFogueira()
    {
        itensFire = 0;
        Instantiate(finalFogueira, finalPositionFogueira.position, finalPositionFogueira.rotation);
        Info("Now take what you collected back to the Oca and ligth the firepit!");        
    }
}
