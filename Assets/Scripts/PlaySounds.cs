﻿using UnityEngine;
using System.Collections;

public class PlaySounds : MonoBehaviour
{
    public AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public bool Play(AudioClip audio)
    {
        if (audio == null)
            return false;

        if (audioSource.isPlaying)
            audioSource.Stop();
        audioSource.clip = audio;
        audioSource.Play();
        return true;
    }
}
