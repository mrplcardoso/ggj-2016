﻿using UnityEngine;
using System.Collections;

public class Flight : MonoBehaviour
{
  private float forwardVel = 15;
  private  float heightVel = 100;

  void Update()
  {
    StartCoroutine("Jump");
    StartCoroutine("MoveForward");
    StartCoroutine("Rotate");
  }

  IEnumerator Jump()
  {
    if(Input.GetKeyDown(KeyCode.Space))
    {
      GetComponent<Rigidbody>().velocity = Vector3.zero;
      GetComponent<Rigidbody>().AddForce(Vector3.up * heightVel * Time.deltaTime, ForceMode.Impulse);
    }
    yield return null;
  }

  IEnumerator MoveForward()
  {
    if(Input.GetKey(KeyCode.W))
    {
      transform.Translate(Vector3.forward * forwardVel * Time.deltaTime);
    }
    if(Input.GetKey(KeyCode.S))
    {
      transform.Translate(Vector3.back * forwardVel * Time.deltaTime);
    }
    yield return null;
  }

  IEnumerator Rotate()
  {
    if(Input.GetKey(KeyCode.D))
    {
      transform.Rotate(Vector3.up * 20 *Time.deltaTime);
    }
    if(Input.GetKey(KeyCode.A))
    {
      transform.Rotate(Vector3.down * 20 * Time.deltaTime);
    }
    yield return null;
  }
}
