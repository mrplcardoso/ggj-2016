﻿using UnityEngine;
using System.Collections;

public class create : MonoBehaviour {

    public GameObject[] itens;


	// Use this for initialization
	void Start () {
        foreach (GameObject iten in itens)
        {
            GameObject i = Instantiate(iten, Vector3.zero, Quaternion.identity) as GameObject;
            i.transform.eulerAngles = new Vector3(Random.Range(1, 360), Random.Range(1, 360), Random.Range(1, 360));
        }
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}
